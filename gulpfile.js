// Run using 'gulp vet --verbose' to display files processed, or just 'gulp vet'
// Problem running when js file is gulp.js. Had to rename to gulpfile.js
// http://stackoverflow.com/questions/22461440/gulp-fails-with-message-object-expected

var gulp = require('gulp');
var args = require('yargs').argv;    // Tool to get the console argument. This is used as first param of gulpif().
var config = require('./gulp.config')();

/* Gulp plugins */
// This plugin takes the gulp plugin names and creates a variable for it (and without the '-'.
var $ = require('gulp-load-plugins')( {lazy : true});

// var gulpprint = require('gulp-print');
// var jshint  = require('gulp-jshint');
// var jscs = require('gulp-jscs');
// var util = require('gulp-util');
// var gulpif = require('gulp-if'); 


gulp.task('vet', function() {
    log('Analyzing source with JSHint and JSCS.');
    
    // Pipe in the source as stream to Gulp
    return gulp.src(config.alljs)
               .pipe($.if(args.verbose, $.print()))
               //.pipe(jscs())
               .pipe($.jshint())
               .pipe($.jshint.reporter('jshint-stylish', { verbose: true }))    // Tell jshint how to behave. e.g. use jshint-stylish
               .pipe($.jshint.reporter('fail'));  // This will fail if jshint finds an error.
});

////////////////

function log(msg) {
    if(typeof(msg) === 'object') {
        for(var item in msg) {
            if(msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    }
    else {
        $.util.log($.util.colors.blue(msg));
    }
}