// Listing globally installed packages in npm
1) npm list -g --depth=0

'dependencies':
    -save saves it to 'dependencies' in package.json
    -needed at runtime
    -e.g. Express, Angular, Bootstrap

'devDependencies':
    // save-dev saves it to 'devDependencies' in package.json
    -needed during development
    -e.g. concat, uglify, JSHint

// To install Gulp, go to the right folder.
1) npm init
2) npm install gulp --save-dev

// Setting up jscs and jshint
1) npm install --save-dev gulp-jscs gulp-jshint