(function(){
    'use strict';
    
    // You can tell jsHint to ignore a particular warning using /* jshint -W033 */, where W033 is the error code.
    function testFunction() {
        return {
            test: 'test'
        };
    }    
})();