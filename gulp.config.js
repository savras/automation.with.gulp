// Node module export
module.exports = function() {    
    // All js that we want to vet.
    var config = {
        alljs: [                    
            './src/**/*.js',
            './src/*.js',
            './*.js'
        ]
    };    
    return config;
};
